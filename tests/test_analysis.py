from pathlib import Path

from sonnen.analysis.main import run, Options


def test_analysis(tmp_path: Path):
    input_file = tmp_path / "input.csv"
    output_file = tmp_path / "output.csv"
    with open(input_file, "w") as f:
        lines = [
            "timestamp;serial;grid_purchase;grid_feedin;direct_consumption;date",
            "2023-05-22T15:40:00.000Z;1711356005;673;0;null;2023-05-22",
            "2023-05-22T15:54:00.000Z;2105167400;n/a;36;null;2023-05-22",
            "2023-05-22T16:14:00.000Z;1083091999;7012;0;null;2023-05-22",
            "2023-05-22T04:57:00.000Z;970568993;0;2045;null;2023-05-22",
            "2023-05-22T16:30:00.000Z;1559834999;27;0;null;2023-05-22",
        ]
        f.writelines(line + "\n" for line in lines)
    run(options=Options(input=input_file, output=output_file))
    with open(output_file, "r") as f:
        result = f.read().splitlines()
    assert result == [
        "hour,grid_purchase_per_hour,grid_feedin_per_hour,is_highest",
        "4,0,2045,true",
        "15,673,36,false",
        "16,7039,0,false",
    ]


def test_duplicates(tmp_path: Path):
    input_file = tmp_path / "input.csv"
    output_file = tmp_path / "output.csv"
    with open(input_file, "w") as f:
        lines = [
            "timestamp;serial;grid_purchase;grid_feedin;direct_consumption;date",
            "2023-05-22T15:40:00.000Z;1711356005;673;0;null;2023-05-22",
            "2023-05-22T15:54:00.000Z;2105167400;n/a;36;null;2023-05-22",
            "2023-05-22T16:14:00.000Z;1083091999;7012;0;null;2023-05-22",
            "2023-05-22T15:40:00.000Z;1711356005;673;0;null;2023-05-22",
            "2023-05-22T04:57:00.000Z;970568993;0;2045;null;2023-05-22",
            "2023-05-22T16:30:00.000Z;1559834999;27;0;null;2023-05-22",
            "2023-05-22T15:54:00.000Z;2105167400;n/a;36;null;2023-05-22",
            "2023-05-22T16:14:00.000Z;1083091999;7012;0;null;2023-05-22",
        ]
        f.writelines(line + "\n" for line in lines)
    run(options=Options(input=input_file, output=output_file))
    with open(output_file, "r") as f:
        result = f.read().splitlines()
    assert result == [
        "hour,grid_purchase_per_hour,grid_feedin_per_hour,is_highest",
        "4,0,2045,true",
        "15,673,36,false",
        "16,7039,0,false",
    ]


def test_invalid_values(tmp_path: Path):
    input_file = tmp_path / "input.csv"
    output_file = tmp_path / "output.csv"
    with open(input_file, "w") as f:
        lines = [
            "timestamp;serial;grid_purchase;grid_feedin;direct_consumption;date",
            "2023-05-22T15:40:00.000Z;1711356005;673;0;null;2023-05-22",
            "2023-05-22T15:54:00.000Z;2105167400;n/a;36;null;2023-05-22",
            "2023-05-22T16:14:00.000Z;1083091999;7012;0;null;2023-05-22",
            "2023-05-22T04:57:00.000Z;970568993;0;2045;null;2023-05-22",
            "2023-05-22T16:30:00.000Z;1559834999;27;0;null;2023-05-22",
            "2023-05-22T16:14:00.000Z;1083091999;invalid;0;null;2023-05-22",
            "2023-05-22T04:57:00.000Z;invalid;0;2045;null;2023-05-22",
            "2023-05-22T16:30:00.000Z;1559834999;27;0;invalid;2023-05-22",
        ]
        f.writelines(line + "\n" for line in lines)
    run(options=Options(input=input_file, output=output_file))
    with open(output_file, "r") as f:
        result = f.read().splitlines()
    assert result == [
        "hour,grid_purchase_per_hour,grid_feedin_per_hour,is_highest",
        "4,0,4090,true",
        "15,673,36,false",
        "16,7039,0,false",
    ]
