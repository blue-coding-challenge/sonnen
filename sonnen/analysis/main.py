#!/usr/bin/env python3
from dataclasses import dataclass
from pathlib import Path
import logging
import argparse

import duckdb


logger = logging.getLogger(__name__)


@dataclass
class Options:
    input: Path
    output: Path


def run(options: Options):
    """
    Ingests the measurements into an in-memory DuckDB.
    Cleans the resulting dataset and performs hourly aggregation.
    """
    logger.info(f"Options: {options}")
    assert options.input.exists(), f"File {options.input} does not exist"
    assert (
        options.output.parent.exists()
    ), f"File {options.output.parent} does not exist"
    con = duckdb.connect(":memory:")
    con.sql(
        f"CREATE TEMP TABLE measurements_raw AS SELECT * FROM read_csv_auto('{options.input}')"
    )
    imported_rows = con.sql("SELECT COUNT(*) FROM measurements_raw").fetchone()[0]
    logger.info(f"Imported {imported_rows} rows")
    con.sql(
        """CREATE TEMP TABLE measurements_cleaned AS SELECT DISTINCT * FROM (SELECT
           * EXCLUDE(grid_feedin, grid_purchase, direct_consumption),
           CAST(grid_purchase AS INTEGER) as grid_purchase,
           CAST(grid_feedin AS INTEGER) as grid_feedin,
           CAST(direct_consumption AS INTEGER)  as direct_consumption
       FROM (
       SELECT
           * EXCLUDE(grid_purchase, direct_consumption),
           NULLIF(grid_purchase, 'n/a') as grid_purchase,
           NULLIF(direct_consumption, 'null') as direct_consumption
       FROM measurements_raw)
       WHERE TRY_CAST(IFNULL(grid_purchase, 0) AS INTEGER) IS NOT NULL
       AND TRY_CAST(IFNULL(direct_consumption, 0) AS INTEGER) IS NOT NULL)"""
    )
    cleaned_rows = con.sql("SELECT COUNT(*) FROM measurements_cleaned").fetchone()[0]
    logger.info(
        f"Filtered input to {cleaned_rows} rows, filtered {imported_rows - cleaned_rows} rows"
    )
    con.sql("DROP TABLE measurements_raw")
    con.sql(
        """CREATE TEMP TABLE hourly_grid_measurements AS (
       SELECT
           HOUR(timestamp) as hour,
           SUM(grid_purchase) as grid_purchase_per_hour,
           SUM(grid_feedin) as grid_feedin_per_hour
       FROM measurements_cleaned
       GROUP BY hour)"""
    )
    con.sql("DROP TABLE measurements_cleaned")
    con.sql(
        """CREATE TEMP TABLE highest_hourly_grid_measurements AS (
       SELECT
           hour,
           grid_purchase_per_hour,
           grid_feedin_per_hour,
           CASE WHEN ROW_NUMBER() OVER (ORDER BY grid_feedin_per_hour DESC) = 1 THEN TRUE ELSE FALSE END AS is_highest
       FROM hourly_grid_measurements
       ORDER BY hour)
       """
    )
    con.sql("DROP TABLE hourly_grid_measurements")
    con.sql(
        f"""COPY (SELECT * FROM highest_hourly_grid_measurements)
               TO '{options.output}' (HEADER, DELIMITER ',')"""
    )
    logger.info(f"Exported hourly grid measurements to {options.output}")


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        prog="main",
        description="Import measurements and output aggregated hourly measurements.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-i",
        "--input",
        help="input measurements",
        default="data/measurements.csv",
        type=Path,
    )
    parser.add_argument(
        "-o",
        "--output",
        help="output hourly aggregation table",
        default="data/highest_hourly_grid.csv",
        type=Path,
    )
    args = parser.parse_args()
    run(Options(input=args.input, output=args.output))


if __name__ == "__main__":
    main()
