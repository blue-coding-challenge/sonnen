#!/usr/bin/env bash

set -euo pipefail

IMAGE_NAME="sonnen-analysis"
IMAGE_VERSION="0.1.0"
IMAGE="$IMAGE_NAME:$IMAGE_VERSION"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR"

docker build -t "$IMAGE" .
docker run --rm -v "$PWD/data:/src/data" "$IMAGE" "$@"
