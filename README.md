# Sonnen Challenge

## Setup

1) [Install Poetry through the official installer](https://python-poetry.org/docs/)

2) Set up a new virtualenv with Poetry:

```sh
poetry install
```

## Analysis

### Run local

```sh
poetry run analysis
```

With different arguments:

```sh
poetry run analysis -i "measurements.csv" -o "export.csv"
```

### Run via Docker

```sh
./run.sh
```

The same arguments can be used:

```sh
./run.sh -i "measurements.csv" -o "export.csv"
```

### Options

```sh
usage: main [-h] [-i INPUT] [-o OUTPUT]

Import measurements and output aggregated hourly measurements.

options:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        input measurements (default: data/measurements.csv)
  -o OUTPUT, --output OUTPUT
                        output hourly aggregation table (default: data/highest_hourly_grid.csv)
```

## Tests

Tests can be run with:

```sh
poetry run pytest
```

## Install pre-commit hook

The pre-commit hooks can be installed with:

```sh
pre-commit install
```